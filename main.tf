resource "aws_organizations_policy" "ai_opt_out_policy" {
  name        = "ai_opt_out_policy"
  type        = "AISERVICES_OPT_OUT_POLICY"
  description = "Policy that opts out of storing and processing customer data by AI Services."

  content = templatefile("${path.module}/templates/opt_out_all.tpl", {})

  tags = local.tags
}

resource "aws_organizations_policy_attachment" "ai_opt_out" {
  for_each = toset(var.apply_to_ous_or_accounts)

  policy_id = aws_organizations_policy.ai_opt_out_policy.id
  target_id = each.value
}
