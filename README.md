# Module: Organizations AI OptOut Policies

## Overview

This Module creates an AI Optout Policy for the AWS Organization.
Certain AWS artificial intelligence (AI) services may store and use customer content processed by those services for the development and continuous improvement of Amazon AI services and technologies. As an AWS customer, you can choose to opt out of having your content stored or used for service improvements. Instead of configuring this setting individually for each AWS account that your organization uses, you can configure an organization policy that enforces your setting choice on all accounts that are members of the organization.

## Details

The Policy sets the default for the organization to opt out of all AI services. This automatically includes any AI services not otherwise explicitly exempted, including any AI services that AWS might deploy in the future. You can attach child policies to OUs or directly to accounts to override this setting for any AI service.

## Usage

```
module "organizations_ai_optout" {
  source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-organizations-ai-optout.git?ref=master"

  tags = var.tags
}
```

## Info

Make sure you add "AISERVICES_OPT_OUT_POLICY" to your `enabled_policy_types` in your `aws_organizations_organization` definition.
Using the AWS Terraform Landingzone you can add the Policy Type to `organizations_enabled_policies` in your variables file.

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_organizations_policy.ai_opt_out_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy) | resource |
| [aws_organizations_policy_attachment.ai_opt_out](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy_attachment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_apply_to_ous_or_accounts"></a> [apply\_to\_ous\_or\_accounts](#input\_apply\_to\_ous\_or\_accounts) | A list of AWS Organization OU IDs or AWS Account IDs that should have the Policies applied (can be empty). | `list(string)` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags for all AWS Resources | `map` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
