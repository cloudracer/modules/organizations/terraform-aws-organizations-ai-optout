locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-organizations-ai-optout"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-organizations-ai-optout"
  }
  tags = merge(local.tags_module, var.tags)
}
