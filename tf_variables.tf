variable "apply_to_ous_or_accounts" {
  type        = list(string)
  description = "A list of AWS Organization OU IDs or AWS Account IDs that should have the Policies applied (can be empty)."
  default     = []
}

variable "tags" {
  description = "Tags for all AWS Resources"
  default     = {}
}
